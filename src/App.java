public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("JBR2.30");

        Customer customer1 = new Customer(1, "Giang", 10);
        Customer customer2 = new Customer(2, "Linh", 40);

        customer1.toString();
        customer2.toString();

        System.out.println(customer1);
        System.out.println(customer2);

        Invoice invoice1 = new Invoice(10, customer1, 20000);
        Invoice invoice2 = new Invoice(11, customer2, 40000);

        invoice1.getCustomerID();
        invoice2.getCustomerID();

        invoice1.getCustomerName();
        invoice2.getCustomerName();

        invoice1.getCustomerDiscount();
        invoice2.getCustomerDiscount();

        
        invoice1.getAmountAfterDiscount();
        invoice2.getAmountAfterDiscount();

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
    }
}
